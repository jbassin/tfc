/* eslint-disable prefer-destructuring, no-case-declarations, prefer-template */

/***********************************************************************************************************************
 * Requires
 **********************************************************************************************************************/
const fs = require('fs');
const r = require('ramda');
const peg = require('pegjs');

/***********************************************************************************************************************
 * Compress
 **********************************************************************************************************************/
const compress = r.reduce(
  (acc, con) => (
    r.head(con) === '#' || r.head(r.last(acc)) === '#'
      ? [...acc, con]
      : [...r.init(acc), `${r.last(acc)}\n${con}`]
  ), []
);

/***********************************************************************************************************************
 * Convert
 **********************************************************************************************************************/
const convert = r.map((line) => {
  if (r.head(line) === '#') {
    return {
      type: 'header',
      depth: r.pipe(r.split('#'), r.length)(line) - 1,
      value: r.pipe(r.split('#'), r.last, r.split('['), r.head, r.trim)(line),
      content: null,
      links: r.pipe(
        r.split('#'),
        r.last,
        r.split('['),
        r.tail,
        r.head,
        item => (item || ''),
        r.init, r.split(','),
        r.map(r.trim),
        r.filter(item => r.length(item))
      )(line),
      children: [],
    };
  }
  return {
    type: 'content',
    value: line,
  };
});

/***********************************************************************************************************************
 * Register
 **********************************************************************************************************************/
const linkMap = {
  dict: {},
  registerAlias: (name, alias) => { linkMap.dict[alias] = name; },
  getKeys: () => r.keys(linkMap.dict),
  getValues: () => r.values(linkMap.dict),
  getLink: alias => linkMap.dict[alias],
};

const register = r.curry((registerFunction, lines) => {
  r.forEach((line) => {
    if (line.type === 'header') {
      r.forEach((link) => {
        registerFunction(line.value, link);
      })(line.links);
      registerFunction(line.value, line.value);
    }
  })(lines);
  return lines;
});

/***********************************************************************************************************************
 * Format
 **********************************************************************************************************************/
const parse = peg.generate(fs.readFileSync(`${__dirname}/wiki.peg`, 'utf-8')).parse;
const format = r.map(line => (line.type === 'content' ? r.evolve({ value: parse }, line) : line));

/***********************************************************************************************************************
 * Linker
 **********************************************************************************************************************/
const linker = r.map((item) => {
  if (item.type === 'header') {
    return item;
  }
  const modify = item;
  modify.value = linkify(modify.value);
  return modify;
});
const linkify = text => r.reduce((acc, con) => r.replace(new RegExp('(' + con[0] + ')(?![}@])', 'g'), `{${con[0]}@${r.replace(/\s+/g, '_', con[1])}}`, acc), text, r.zip(linkMap.getKeys(), linkMap.getValues()));

/***********************************************************************************************************************
 * Correct
 **********************************************************************************************************************/
const correct = r.map(item => ((item.type === 'header') ? item : correcter(item)));

const correcter = (content) => {
  if (r.is(Array, content.value)) {
    if (content.type === 'text') {
      return {
        type: 'entry',
        value: content.value,
      };
    }

    const modify = content;
    modify.value = r.map(correcter, modify.value);
    return modify;
  }
  return content;
};

/***********************************************************************************************************************
 * Consolidate
 **********************************************************************************************************************/
const consolidate = r.reduce((acc, con) => {
  switch (true) {
    case (con.type === 'content'):
      return [con, ...acc];
    case (r.isEmpty(acc)):
      return [con];
    case (acc[0].type === 'content'):
      const contentHeader = con;
      contentHeader.content = acc[0].value;
      return [contentHeader, ...r.tail(acc)];
    default:
      const children = r.takeWhile(i => i.depth > con.depth, acc);
      if (r.isEmpty(children)) {
        return [con, ...acc];
      }
      const header = con;
      header.children = consolidate(r.reverse(children));
      return [header, ...r.dropWhile(i => i.depth > con.depth, acc)];
  }
}, []);

/***********************************************************************************************************************
 * Map
 **********************************************************************************************************************/
const entryMap = {
  dict: {},
  registerNode: (title, entry) => { entryMap.dict[title] = entry; return entry; },
  checkEntry: title => (r.includes(title, r.keys(entryMap.dict))),
  getEntryUnedited: title => (entryMap.checkEntry(title) ? entryMap.dict[title] : null),
  getEntry: title => (entryMap.checkEntry(title) ? cessor(entryMap.getEntryUnedited(title)) : null),
  getKeys: () => r.keys(entryMap.dict),
};

const map = r.forEach(r.curry((registerFunc, node) => {
  registerFunc(node.value, {
    title: node.value,
    content: node.content,
  });
  map(node.children);
})(entryMap.registerNode));

/***********************************************************************************************************************
 * Cessor
 **********************************************************************************************************************/
const cessor = (node) => {
  const keys = entryMap.getKeys();
  const index = r.findIndex(item => node.title === item, keys);

  let prev = null;
  if (index > 0) {
    const prevEntry = entryMap.getEntryUnedited(keys[index - 1]);
    if (prevEntry.content === null) {
      prev = cessor(prevEntry).pagination.prev;
    } else {
      prev = {
        link: `/wiki/${r.replace(/\s+/g, '_', prevEntry.title)}`,
        value: prevEntry.title,
      };
    }
  }

  let next = null;
  if (index < r.length(keys) - 1) {
    const nextEntry = entryMap.getEntryUnedited(keys[index + 1]);
    if (nextEntry.content === null) {
      next = cessor(nextEntry).pagination.next;
    } else {
      next = {
        link: `/wiki/${r.replace(/\s+/g, '_', nextEntry.title)}`,
        value: nextEntry.title,
      };
    }
  }

  return {
    ...node,
    breadcrumb: getBreadcrumb(node.title),
    pagination: {
      prev,
      next,
    },
  };
};

/***********************************************************************************************************************
 * TOC
 **********************************************************************************************************************/
const toc = r.curry((parent, item) => r.map(node => ({
  title: node.value,
  parent,
  children: toc(node.value, node.children),
}))(item));

/***********************************************************************************************************************
 * TOC
 **********************************************************************************************************************/
const breadcrumbMap = {
  dict: {},
  registerBreadcrumb: (name, parent) => { breadcrumbMap.dict[name] = parent; },
  getBreadcrumb: name => breadcrumbMap.dict[name],
};

const breadcrumb = r.curry((registerFunc, items) => r.map((node) => {
  registerFunc(node.title, node.parent);
  breadcrumb(registerFunc, node.children);
  return node;
})(items));

const getBreadcrumb = (title) => {
  const parent = breadcrumbMap.getBreadcrumb(title);
  if (parent) {
    return r.flatten([...getBreadcrumb(parent), title]);
  }
  return [title];
};

/***********************************************************************************************************************
 * Pipeline
 **********************************************************************************************************************/
const pipeline = r.pipe(
  r.split('\n'),
  compress,
  convert,
  register(linkMap.registerAlias),
  linker,
  format,
  correct,
  r.reverse,
  consolidate,
  map,
);

/***********************************************************************************************************************
 * Preparation
 **********************************************************************************************************************/
const file = fs.readFileSync(`${__dirname}/inscripta.wiki`, 'utf-8');

const wiki = pipeline(file);
const TOC = toc(null, wiki);
breadcrumb(breadcrumbMap.registerBreadcrumb, TOC);

/***********************************************************************************************************************
 * Exports
 **********************************************************************************************************************/
module.exports = {
  getEntry: entryMap.getEntry,
  toc: TOC,
};
