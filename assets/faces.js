/***********************************************************************************************************************
 * Requires
 **********************************************************************************************************************/
const fs = require('fs');
const r = require('ramda');

/***********************************************************************************************************************
 * Preparation
 **********************************************************************************************************************/
const file = fs.readFileSync(`${__dirname}/info.faces`, 'utf-8');

/***********************************************************************************************************************
 * Parsing
 **********************************************************************************************************************/
const splitFile = r.pipe(
  r.split('\n'),
  r.reject(r.isEmpty),
  r.splitEvery(9),
)(file);

/***********************************************************************************************************************
 * Conversion
 **********************************************************************************************************************/
const toObjects = r.map(
  arr => ({
    name: arr[0],
    job: arr[1],
    dim: {
      alive: arr[2] !== 'x',
      age: arr[2],
      height: arr[3],
      gender: arr[4],
      race: arr[5],
      desc: arr[6],
    },
    bio: arr[7],
    faction: arr[8],
  }),
  splitFile,
);

/***********************************************************************************************************************
 * Faction Color Generation
 **********************************************************************************************************************/
const factions = r.uniq(r.map(r.prop('faction'), toObjects));
const generatePastel = () => `hsl(${360 * Math.random()}, ${25 + 70 * Math.random()}%, ${85 + 10 * Math.random()}%)`;
const colorMap = r.reduce((a, c) => ({ ...a, [c]: generatePastel() }), {}, factions);
const faces = r.map(i => ({ ...i, color: colorMap[i.faction] }), toObjects);

/***********************************************************************************************************************
 * Exports
 **********************************************************************************************************************/
module.exports = faces;
