const axios = require('axios');

export default ({ app, isDev, }, inject) => {
  // eslint-disable-next-line require-await
  inject('get', async (path, params = {}) => axios.get(`${isDev ? 'http://localhost:3000' : 'https://the-floating-cup.herokuapp.com'}/api${path}`, { params, }));
};
