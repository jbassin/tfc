const express = require('express');
const r = require('ramda');
const cors = require('cors');
const routes = require('./routes');

const app = express();

app.use(cors());
app.get('/echo', (req, res) => {
  res.json(routes);
});

r.map(file => file(app), routes);

module.exports = {
  path: '/api',
  handler: app
};
