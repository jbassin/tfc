const undermountain = require('../../assets/undermountain');
const colors = require('../../assets/floor_collections');

module.exports = (app) => {
  app.get('/undermountain', (req, res) => {
    res.send(undermountain);
  });

  app.get('/undermountain/colors', (req, res) => {
    res.send(colors);
  });
};
