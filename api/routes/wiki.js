const r = require('ramda');
const wiki = require('../../assets/inscripta');

module.exports = (app) => {
  app.get('/wiki/toc', (req, res) => {
    res.send(wiki.toc);
  });

  app.get('/wiki/entry', (req, res) => {
    const entry = r.replace(/_/g, ' ', req.query.entry || '');
    res.send(wiki.getEntry(entry === '' ? 'Foreword' : entry));
  });

  app.get('/wiki/breadcrumb', (req, res) => {
    const breadcrumb = r.replace(/_/g, ' ', req.query.breadcrumb || '');
    res.send(wiki.getEntry(breadcrumb === '' ? 'Foreword' : breadcrumb).breadcrumb);
  });
};
