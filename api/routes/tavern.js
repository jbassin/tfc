const tavern = require('../../assets/tavern');

module.exports = (app) => {
  app.get('/tavern', (req, res) => {
    res.send(tavern);
  });
};
