const r = require('ramda');
const lunr = require('elasticlunr');
const faces = require('../../assets/faces');

// eslint-disable-next-line func-names,no-unused-vars
const searcher = lunr(function () {
  this.setRef('name');
  this.addField('name');
  this.addField('job');
  this.addField('faction');
});
r.forEach((face) => { searcher.addDoc(face); }, faces);

const find = name => r.find(r.propEq('name', name), faces);

module.exports = (app) => {
  app.get('/faces', (req, res) => {
    res.send(faces);
  });

  app.get('/faces/group', ({ query: { group } }, res) => {
    if (!group) {
      res.send(faces);
    }

    res.send(r.filter(r.propEq('faction', group), faces));
  });

  app.get('/faces/search', ({ query: { search } }, res) => {
    res.send(
      r.map(
        ({ ref }) => find(ref),
        searcher.search(search)
      )
    );
  });
};
