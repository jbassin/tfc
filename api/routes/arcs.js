const arcs = require('../../assets/arcs');

module.exports = (app) => {
  app.get('/arcs', (req, res) => {
    res.send(arcs.arcs);
  });

  app.get('/arcs/calendar', (req, res) => {
    res.send(arcs.calendar);
  });
};
