const factions = require('../../assets/factions');

module.exports = (app) => {
  app.get('/factions', (req, res) => {
    res.send(factions);
  });
};
