const fs = require('fs');
const r = require('ramda');

const files = fs.readdirSync(`${__dirname}/`);
const transform = r.pipe(
  r.filter(file => file.match(/\.js$/) !== null),
  r.filter(file => file !== 'index.js'),
  r.map(file => file.replace('.js', '')),
  r.map(file => require(`./${file}`)),
);

const retVal = transform(files);
module.exports = retVal;
